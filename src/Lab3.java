import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Main driver of entire polynomial list implementation Lab
 * main() parses the two input files. One for polynomials, and the 2nd for
 * the values that will get input into them.
 * 
 * The last part of main is a hardcoded, particularly ugly, lengthy section 
 * of print logic to specified output file. 
 * @author clark
 *
 */
public class Lab3 {

	public static void main(String[] args) throws IOException {

		// check arguments before anything else
		if (args.length != 3) {
			System.out.println("Invalid number of arguments");
			System.exit(-1);
		}

		// declarations
		FileReader expressionsFile = null;
		FileReader valuesFile = null;
		FileWriter outputFile = null;
		MetricsManager metrics = new MetricsManager();

		int input[] = new int[100];

		try {
			expressionsFile = new FileReader(args[0]);
			valuesFile = new FileReader(args[1]);
			outputFile = new FileWriter(args[2]);

			int c;
			int size = 0;
			int idx = 0;
			int next = '+';
			Polynomial expression[] = new Polynomial[4];
			

			// read input file until newline is found.
			metrics.tic();
			while ((c = expressionsFile.read()) != -1) {

				if (c != 32 && c != 9) {
					input[size] = c;
					size++;
				}

				if (expression[idx] == null)
					expression[idx] = new Polynomial();

				// we've reached end of term
				if (c == '+' || c == '-' || c == '\n') {

					// put input into a properly sized array
					int term[] = new int[size - 1];
					for (int i = 0; i < size - 1; i++) {
						term[i] = input[i];
					}
					if (next == '+' || next == '\n')
						expression[idx].addTerm(term);
					if (next == '-')
						expression[idx].subtractTerm(term);
					if (c == '\n')
						idx++;

					next = c;
					size = 0;
				}

			} // while
			
			// stream the values file into a matrix
			int values[] = new int[10];
			int xyz[][] = new int[4][3];
			int sign = 1;
			idx = 0;
			while ((c = valuesFile.read()) != -1) {

				if (c != 32 && c != 9) {
					values[size] = c;
					size++;
				}

				if (c == '\n') {
					int j = 0;
					for (int i = 1; j < 3; i++) {
						if (values[i] == '-') {
							sign = -1;
							i++;
						}
						if (values[i] > 47 && values[i] < 58) {
							xyz[idx][j] = sign * (values[i] - '0');
							j++;
						}
						sign = 1;
					}
					size = 0;
					idx++;
				}
			}
			
/*********** Hardcoded Output Section: Print the parsed polynomials and Eval to the outputFile ***********/
			Polynomial resultingExp[] = new Polynomial[15]; //15 required operations
			PolyMath polyMath = new PolyMath();
			
			//output parsed polynomials from input file
			outputFile.write("Input Polynomials\nA) ");
			expression[0].polyPrint(outputFile);
			outputFile.write("B) ");
			expression[1].polyPrint(outputFile);
			outputFile.write("C) ");
			expression[2].polyPrint(outputFile);
			outputFile.write("D) ");
			expression[3].polyPrint(outputFile);
			outputFile.write("______________________________________\n\n");

			// Use letters for readability
			Polynomial A = expression[0];
			Polynomial B = expression[1];
			Polynomial C = expression[2];
			Polynomial D = expression[3];
			
			//calculate the new polynomials and store in new object
			resultingExp[0] = polyMath.add(A, B);
			resultingExp[1] = polyMath.add(A, C);
			resultingExp[2] = polyMath.add(A, D);
			resultingExp[3] = polyMath.add(B, C);
			resultingExp[4] = polyMath.add(B, D);
			resultingExp[5] = polyMath.add(C, D);
			resultingExp[6] = polyMath.subtract(B, A);
			resultingExp[7] = polyMath.subtract(B, D);
			resultingExp[8] = polyMath.multiply(A, B);
			resultingExp[9] = polyMath.multiply(A, C);
			resultingExp[10] = polyMath.multiply(A, D);
			resultingExp[11] = polyMath.multiply(B, A);
			resultingExp[12] = polyMath.multiply(B, C);
			resultingExp[13] = polyMath.multiply(B, D);
			resultingExp[14] = polyMath.multiply(C, D);
			metrics.toc();
			
			// print evaluation results for each expression, for each input data
			int result;
			outputFile.write("A+B) ");
			resultingExp[0].polyPrint(outputFile);
			for( int i = 0; i<4; i++) {
				result = resultingExp[0].evaluate(xyz[i][0], xyz[i][1], xyz[i][2]);
				outputFile.write("= " + result + "\twhen x=" + xyz[i][0] + ", y=" + xyz[i][1] + ", z=" + xyz[i][2] + "\n");
			}
			
			outputFile.write("\nA+C) ");
			resultingExp[1].polyPrint(outputFile);
			for( int i = 0; i<4; i++) {
				result = resultingExp[1].evaluate(xyz[i][0], xyz[i][1], xyz[i][2]);
				outputFile.write("= " + result + "\twhen x=" + xyz[i][0] + ", y=" + xyz[i][1] + ", z=" + xyz[i][2] + "\n");
			}
			
			outputFile.write("\nA+D) ");
			resultingExp[2].polyPrint(outputFile);
			for( int i = 0; i<4; i++) {
				result = resultingExp[2].evaluate(xyz[i][0], xyz[i][1], xyz[i][2]);
				outputFile.write("= " + result + "\twhen x=" + xyz[i][0] + ", y=" + xyz[i][1] + ", z=" + xyz[i][2] + "\n");
			}
			
			outputFile.write("\nB+C) ");
			resultingExp[3].polyPrint(outputFile);
			for( int i = 0; i<4; i++) {
				result = resultingExp[3].evaluate(xyz[i][0], xyz[i][1], xyz[i][2]);
				outputFile.write("= " + result + "\twhen x=" + xyz[i][0] + ", y=" + xyz[i][1] + ", z=" + xyz[i][2] + "\n");
			}
			
			outputFile.write("\nB+D) ");
			resultingExp[4].polyPrint(outputFile);
			for( int i = 0; i<4; i++) {
				result = resultingExp[4].evaluate(xyz[i][0], xyz[i][1], xyz[i][2]);
				outputFile.write("= " + result + "\twhen x=" + xyz[i][0] + ", y=" + xyz[i][1] + ", z=" + xyz[i][2] + "\n");
			}
			
			outputFile.write("\nC+D) ");
			resultingExp[5].polyPrint(outputFile);
			for( int i = 0; i<4; i++) {
				result = resultingExp[5].evaluate(xyz[i][0], xyz[i][1], xyz[i][2]);
				outputFile.write("= " + result + "\twhen x=" + xyz[i][0] + ", y=" + xyz[i][1] + ", z=" + xyz[i][2] + "\n");
			}
			
			outputFile.write("\nB-A) ");
			resultingExp[6].polyPrint(outputFile);
			for( int i = 0; i<4; i++) {
				result = resultingExp[6].evaluate(xyz[i][0], xyz[i][1], xyz[i][2]);
				outputFile.write("= " + result + "\twhen x=" + xyz[i][0] + ", y=" + xyz[i][1] + ", z=" + xyz[i][2] + "\n");
			}
			
			outputFile.write("\nB-D) ");
			resultingExp[7].polyPrint(outputFile);
			for( int i = 0; i<4; i++) {
				result = resultingExp[7].evaluate(xyz[i][0], xyz[i][1], xyz[i][2]);
				outputFile.write("= " + result + "\twhen x=" + xyz[i][0] + ", y=" + xyz[i][1] + ", z=" + xyz[i][2] + "\n");
			}
			
			outputFile.write("\nA*B) ");
			resultingExp[8].polyPrint(outputFile);
			for( int i = 0; i<4; i++) {
				result = resultingExp[8].evaluate(xyz[i][0], xyz[i][1], xyz[i][2]);
				outputFile.write("= " + result + "\twhen x=" + xyz[i][0] + ", y=" + xyz[i][1] + ", z=" + xyz[i][2] + "\n");
			}
			
			outputFile.write("\nA*C) ");
			resultingExp[9].polyPrint(outputFile);
			for( int i = 0; i<4; i++) {
				result = resultingExp[9].evaluate(xyz[i][0], xyz[i][1], xyz[i][2]);
				outputFile.write("= " + result + "\twhen x=" + xyz[i][0] + ", y=" + xyz[i][1] + ", z=" + xyz[i][2] + "\n");
			}
			
			outputFile.write("\nA*D) ");
			resultingExp[10].polyPrint(outputFile);
			for( int i = 0; i<4; i++) {
				result = resultingExp[10].evaluate(xyz[i][0], xyz[i][1], xyz[i][2]);
				outputFile.write("= " + result + "\twhen x=" + xyz[i][0] + ", y=" + xyz[i][1] + ", z=" + xyz[i][2] + "\n");
			}
			
			outputFile.write("\nB*A) ");
			resultingExp[11].polyPrint(outputFile);
			for( int i = 0; i<4; i++) {
				result = resultingExp[11].evaluate(xyz[i][0], xyz[i][1], xyz[i][2]);
				outputFile.write("= " + result + "\twhen x=" + xyz[i][0] + ", y=" + xyz[i][1] + ", z=" + xyz[i][2] + "\n");
			}
			
			outputFile.write("\nB*C) ");
			resultingExp[12].polyPrint(outputFile);
			for( int i = 0; i<4; i++) {
				result = resultingExp[12].evaluate(xyz[i][0], xyz[i][1], xyz[i][2]);
				outputFile.write("= " + result + "\twhen x=" + xyz[i][0] + ", y=" + xyz[i][1] + ", z=" + xyz[i][2] + "\n");
			}
			
			outputFile.write("\nB*D) ");
			resultingExp[13].polyPrint(outputFile);
			for( int i = 0; i<4; i++) {
				result = resultingExp[13].evaluate(xyz[i][0], xyz[i][1], xyz[i][2]);
				outputFile.write("= " + result + "\twhen x=" + xyz[i][0] + ", y=" + xyz[i][1] + ", z=" + xyz[i][2] + "\n");
			}
			
			outputFile.write("\nC*D) ");
			resultingExp[14].polyPrint(outputFile);
			for( int i = 0; i<4; i++) {
				result = resultingExp[14].evaluate(xyz[i][0], xyz[i][1], xyz[i][2]);
				outputFile.write("= " + result + "\twhen x=" + xyz[i][0] + ", y=" + xyz[i][1] + ", z=" + xyz[i][2] + "\n");
			}
			outputFile.write("\nConversion Time: " + metrics.getRuntime()/1000 + "ms");


		} finally {
			// close the files
			if (expressionsFile != null)
				expressionsFile.close();
			if (valuesFile != null)
				valuesFile.close();
			if (outputFile != null)
				outputFile.close();
		}
	}// main

}
