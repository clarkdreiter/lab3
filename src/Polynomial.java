import java.io.FileWriter;
import java.io.IOException;

/**
 * Polynomial class is the main class for implementing the linked-list
 * data structure. The Term subclass is the node type. Observing this subclass
 * makes it clear that the list is singly linked.
 * 
 * The class also contains useful helper functions used in the main program.
 * @author clark
 * 
 */
public class Polynomial {

	// node class
	public class Term {
		public int scalar;
		public int x;
		public int y;
		public int z;
		Term next_term;

		// node constructor
		public Term() {
			scalar = 1;
			x = 0;
			y = 0;
			z = 0;
			next_term = null;
		}

	}

	public Term Head, Tail;

	// List Constructor
	public Polynomial() {
		Head = null;
	}

	/**
	 * Main function for generating the lists. addTerm appends the input array,
	 * after parsing, to the calling object. Checks for simplification where
	 * available.
	 * 
	 * @param input 7 or 8 element int array (known limitation)
	 */
	public void addTerm(int input[]) {
		// parse the input array into a node, starting with mem alloc
		boolean match = false;
		Term temp = new Term();
		int i = 0;

		// determine scalar value of term
		if (input.length == 7) {
			temp.scalar = input[0] - '0';
			i = 1;
		} else {
			temp.scalar = (input[0] - '0') * 10 + (input[1] - '0');
			i = 2;
		}

		// assign eponentiation values of x,y,z
		temp.x = input[i + 1] - '0';
		temp.y = input[i + 3] - '0';
		temp.z = input[i + 5] - '0';

		// Check if the term already exists.
		// If it does, don't add it to the list, just update the scalar of the
		// pre-existing node
		Term curr = Head;
		while (curr != null) {

			if (curr.x == temp.x && curr.y == temp.y && curr.z == temp.z) {
				curr.scalar += temp.scalar;
				match = true;
				curr = curr.next_term;
			} else
				curr = curr.next_term;
		}

		// assign the next pointer, handling special case of empty list
		if (Head == null) {
			Head = temp;
			Tail = temp;
		} else if (match == false) {
			Tail.next_term = temp;
			Tail = temp;

		}

	}

	/**
	 * Self explanatory. Calls add but mulitplys by -1.
	 * 
	 * @param input 7 or 8 int array containing input text for the polynomial term
	 *              to be added to list
	 */
	public void subtractTerm(int input[]) {

		addTerm(input);
		Tail.scalar *= -1;

	}

	/**
	 * Helper function to mathematically add two Polynomial lists. Implements
	 * simplification where necessary Note: Modifies the calling object's list.
	 * 
	 * @param exp Polynomial type
	 */
	public void addExpression(Polynomial exp) {

		boolean match = false;

		Term add = exp.Head;
		Term prev = Tail;

		do {

			Term curr = Head;
			while (curr != null) {
				if (curr.x == add.x && curr.y == add.y && curr.z == add.z) {
					curr.scalar += add.scalar;
					match = true;
					curr = curr.next_term;
				} else
					curr = curr.next_term;

			}

			if (match == true) {

				// get rid of add (its redundant) by pointing list around it
				if (add == exp.Head)
					exp.Head = exp.Head.next_term;
				if (prev != Tail)
					prev.next_term = add.next_term;

			} else
				prev = add;

			add = add.next_term;
			match = false;
		} while (add != null);

		Tail.next_term = exp.Head;

	}
	
	/**
	 * @param x integer value of x
	 * @param y integer value of y
	 * @param z integer value of z
	 * @return sum of polynomial when evaluated with given params
	 */
	public int evaluate(int x, int y, int z) {
		double x_term, y_term, z_term, sum = 0;

		Term curr = this.Head;

		while (curr != null) {
			x_term = Math.pow((double) x, (double) curr.x);
			y_term = Math.pow((double) y, (double) curr.y);
			z_term = Math.pow((double) z, (double) curr.z);
			sum += (double) curr.scalar * x_term * y_term * z_term;
			curr = curr.next_term;
		}

		return (int) sum;
	}

	/**
	 * Helper function to return a new Polynomial copy of the calling object This is
	 * useful during the expression calculations where the input polynomials need to
	 * be reused and not tampered with during calculations.
	 * 
	 * @return Polynomial
	 */
	public Polynomial polyCopy() {

		Polynomial temp = new Polynomial();
		Term term = new Term();
		temp.Head = term;

		Term curr = this.Head;
		while (curr != null) {
			term.scalar = curr.scalar;
			term.x = curr.x;
			term.y = curr.y;
			term.z = curr.z;
			temp.Tail = term;
			curr = curr.next_term;
			if (curr != null) {
				term.next_term = new Term();
				term = term.next_term;
			}
		}

		return temp;
	}

	/**
	 * Prints the contents of the calling object's list in typical infix notation to
	 * the specified output file object
	 * 
	 * @param outputFile
	 * @throws IOException
	 */
	public void polyPrint(FileWriter outputFile) throws IOException {

		FileWriter file = outputFile;

		Term curr = Head;
		while (curr != null) {

			if (curr.scalar >= 0 && curr != Head)
				file.write("+");

			file.write(curr.scalar + "x" + curr.x + "y" + curr.y + "z" + curr.z);
			curr = curr.next_term;
		}

		file.write("\n");
	}

}
