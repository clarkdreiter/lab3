
/**
 * Creates a type that behaves as a stop watch.
 * Tic, and toc set the times to eval runtime.
 * Objects also keep track of total time throughout tics/tocs
 * @author clark
 *
 */
public class MetricsManager {

	public long startTime;
	public long stopTime;
	public long totalTime;

	public MetricsManager() {
		
		totalTime = 0;
		stopTime = 0;
		startTime = 0;
	}
	
	/**
	 * Copy starting time
	 */
	public void tic() {

		startTime = System.nanoTime();
	}
	
	/**
	 * Copy stopping time and add to totalTime
	 */
	public void toc() {
		
		stopTime = System.nanoTime();
		totalTime = totalTime + (stopTime - startTime);
	}
	
	/**
	 * calculate the difference between
	 * the start time and stop time
	 * @return The difference between start and stop time
	 */
	public long getRuntime() {
		
		return stopTime - startTime;
	}

}
