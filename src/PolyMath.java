
/**
 * This class implements the addition, subtraction, and multiplication
 * of entire polynomial expressions. The member functions use Polynomial objects
 * and creates new objects to avoid tampering input polynomials since they are to
 * be used in many operations.
 * 
 * Extending from Polynomial to have access to the Term class.
 * @author clark
 *
 */
public class PolyMath extends Polynomial {

	/**
	 * Adds two Polynomial lists into a single list.
	 * @param expA
	 * @param expB
	 * @return
	 */
	public Polynomial add(Polynomial expA, Polynomial expB) {
		// add two expressions by looping through, and checking if any match
		Polynomial tempA = new Polynomial();
		Polynomial tempB = new Polynomial();
		tempA = expA.polyCopy();
		tempB = expB.polyCopy();
		tempA.addExpression(tempB);

		return tempA;
	}

	/**
	 * Same concept as add except multiply each node by -1;
	 * @param expA
	 * @param expB
	 * @return
	 */
	public Polynomial subtract(Polynomial expA, Polynomial expB) {

		// subtract B from A
		Polynomial tempA = new Polynomial();
		Polynomial tempB = new Polynomial();
		tempA = expA.polyCopy();
		tempB = expB.polyCopy();

		Term curr = tempB.Head;
		while (curr != null) {
			curr.scalar *= -1;
			curr = curr.next_term;
		}

		tempA.addExpression(tempB);

		return tempA;
	}

	/**
	 * Multiplies two Polynomial expressions.
	 * Simplifies if necessary.
	 * @param expA
	 * @param expB
	 * @return
	 */
	public Polynomial multiply(Polynomial expA, Polynomial expB) {
		// add exponential terms and multiply scalars
		Polynomial tempA = new Polynomial();
		Polynomial tempB = new Polynomial();
		Polynomial productExp = new Polynomial();
		tempA = expA.polyCopy();
		tempB = expB.polyCopy();

		Term mult = tempB.Head;
		boolean match = false;

		do {

			Term curr = tempA.Head;
			while (curr != null) {
				Term temp = new Term();
				temp.scalar = curr.scalar * mult.scalar;
				temp.x = curr.x + mult.x;
				temp.y = curr.y + mult.y;
				temp.z = curr.z + mult.z;

				if (productExp.Head == null) {
					productExp.Head = temp;
					productExp.Tail = temp;
					productExp.Tail.next_term = null;

				}

				else {
					Term check = productExp.Head;

					while (check != null) {
						if (check.x == temp.x && check.y == temp.y && check.z == temp.y) {
							check.scalar += temp.scalar;
							match = true;
						}

						check = check.next_term;
					}

					if (match == false) {
						productExp.Tail.next_term = temp;
						productExp.Tail = temp;
						productExp.Tail.next_term = null;
					}
				}
				match = false;
				curr = curr.next_term;
			}

			mult = mult.next_term;
		} while (mult != null);

		return productExp;
	}

}
